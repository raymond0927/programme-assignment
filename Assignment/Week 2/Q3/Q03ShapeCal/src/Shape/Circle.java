/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Shape;

/**
 *
 * @author itst
 */
public class Circle extends Shape {

    Point center;
    double radius;
    
    
    public Circle(Point center,double radius) {
        super ("Circle");
        this.center = center;
        this.radius = radius;
    }

    public double getRadius(){
        return radius;
    }
    
    public void setRadius(double radius){
        this.radius = radius;
    }
    
    public Point getCenter(){
        return center;
    }
    
    public void setCenter(Point center){
        this.center = center;
    }
    
    
    @Override
    public double getArea() {
        return Math.PI * (radius * radius);
    }
    
       // convert the point into a String representation
    @Override
    public String toString(){
        return "["+center.getX()+", "+center.getY()+"]; Radius = "+radius+"; ";
    }
    
}
