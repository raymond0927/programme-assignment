/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Shape;

/**
 *
 * @author itst
 */
public class Rectangle  extends Shape {
    
    Point topLeft;
    double length;
    double width;
    
    public Rectangle(Point topLeft, double length , double width) {
        super("Rectangle");
        this.topLeft = topLeft;
        this.length = length;
        this.width = width;
    }
    
    public Point getTopLeft(){
        return topLeft;
    }
    
    public void setTopLeft(Point topLeft){
        this.topLeft = topLeft;
    }
    
    public double getLength(){
        return length;
    }
    
    public void setLength(double length){
        this.length = length;
    }

    public double getWidth(){
        return width;
    }
    
    public void setWidth(double width){
        this.width = width;
    }
    
    @Override
    public double getArea() {
        return length*width;
    }
    
           // convert the point into a String representation
    @Override
    public String toString(){
            return "["+topLeft.getX()+", "+topLeft.getY()+"]; Length = "+length+"; Width = "+width;
    }
    
}
