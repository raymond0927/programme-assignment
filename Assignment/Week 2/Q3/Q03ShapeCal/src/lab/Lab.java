/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab;

import Shape.Circle;
import Shape.Point;
import Shape.Rectangle;
import Shape.Shape;
import miscLib.GenLib;

/**
 *
 * @author itst
 */
public class Lab {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
//        System.out.println(GenLib.genInt(8, 20));

        Circle circle;
        Rectangle rectangle;
        
        //Q3a
        circle = new Circle(new Point(22,8), 3.5);
        
        System.out.print(circle.getName()+": "+circle.toString());
        System.out.println("Area = "+circle.getArea()+";");
        
        //Q3b
        rectangle = new Rectangle(new Point(7.5,2), 18, 11);
        
        System.out.print(rectangle.getName()+": "+rectangle.toString());
        System.out.println("; Area = "+rectangle.getArea()+";");
        
//        int randomSize = GenLib.genInt(1, 20);
//        Shape[] shapes = new Shape[randomSize];
//        
//        for (int i =0 ; i < randomSize ; i++){
//            int randomShape = GenLib.genInt(0, 2);
//            System.out.println("shape "+randomShape);
//            if (randomShape > 0){
//                circle = new Circle(new Point(GenLib.genInt(1, 30),GenLib.genInt(1, 30)),GenLib.genInt(1, 30)); 
//                shapes[i] = circle;
//            }else{
//                rectangle = new Rectangle(new Point(GenLib.genInt(1, 30),GenLib.genInt(1, 30)),GenLib.genInt(1, 30), GenLib.genInt(1, 30));
//                shapes[i] = rectangle;
//            }
//        }
//        
//        for (Shape item : shapes){
//            if (item.getName().equalsIgnoreCase("rectangle")){
//                System.out.print(item.getName()+": "+item.toString());
//                System.out.println(" Area = "+item.getArea());
//            }else{
//                System.out.print(item.getName()+": "+item.toString());
//                System.out.println(" Area = "+item.getArea()); 
//            }
//        }
        
    }
    
}
