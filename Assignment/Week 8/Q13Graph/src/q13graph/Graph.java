/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package q13graph;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author user
 */
public class Graph {

    private boolean adjMatrix[][];
    private int numVertices;
    List<Integer> dfsList = new ArrayList(); //record the sequence of DFS
    List<Integer> bfsList = new ArrayList(); //record the sequence of BFS

    public Graph(int numVertices) {
        this.numVertices = numVertices;
        adjMatrix = new boolean[numVertices][numVertices];
    }

    public void addEdge(int i, int j) {        // add the path of two point
        adjMatrix[i][j] = true;
        adjMatrix[j][i] = true;
    }

    public void removeEdge(int i, int j) {        // remove the path of two point
        adjMatrix[i][j] = false;
        adjMatrix[j][i] = false;
    }

    public boolean isEdge(int i, int j) {
        return adjMatrix[i][j];
    }

    public String toString() {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < numVertices; i++) {
            s.append(i + ": ");
            for (boolean j : adjMatrix[i]) {
                s.append((j ? 1 : 0) + " ");
            }
            s.append("\n");
        }
        return s.toString();
    }

    // perform a DFS on the graph G
    public void DFS(int start) {
        if (start >= numVertices || start < 0) {
            System.out.println("Start number is out of range");
        } else {
            boolean[] visited = new boolean[numVertices]; // a visited array to mark which vertices have been visited while doing the DFS

            // do the DFS from start number
            if (!visited[start]) {
                DFS(start, visited);
            }

            System.out.println("DFS start at " + start);
            System.out.print("DFS : ");

            for (int i = 0; i < dfsList.size(); i++) {
                System.out.print(dfsList.get(i) + " ");
            }
            System.out.println("");
        }
    }

    // perform a DFS starting at node at (works recursively)
    public void DFS(int current, boolean[] visited) {
        dfsList.add(current);
        visited[current] = true;
        // recursively visit every node connected to this that we have not already visited
        for (int i = 0; i < numVertices; ++i) {
            if (adjMatrix[current][i] && !visited[i]) {
                DFS(i, visited);
            }
        }
    }

    // perform a BFS on the graph G
    public void BFS(int start) {
        if (start >= numVertices || start < 0) {
            System.out.println("Start number is out of range");
        } else {
            boolean[] visited = new boolean[numVertices]; // a visited array to mark which vertices have been visited while doing the DFS

            // do the BFS from first node not already visited
            if (!visited[start]) {
                bfsList.add(start);
                BFS(start, visited);
            }

            for (int i = 0; i < bfsList.size(); i++) {
                if (!visited[bfsList.get(i)]) {
                    BFS(bfsList.get(i), visited);
                }
            }

            System.out.println("BFS start at " + start);
            System.out.print("BFS : ");

            for (int i = 0; i < bfsList.size(); i++) {
                System.out.print(bfsList.get(i) + " ");
            }
            System.out.println("");
        }
    }

    // perform a BFS starting at node at (works recursively)
    public void BFS(int current, boolean[] visited) {

        visited[current] = true;
        // recursively visit every node connected to this that we have not already visited
        for (int i = 0; i < numVertices; i++) {
            if (adjMatrix[current][i] && !visited[i] && !bfsList.contains(i)) {
                bfsList.add(i);
            }
        }

    }

}
