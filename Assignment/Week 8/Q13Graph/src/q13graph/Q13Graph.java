/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package q13graph;

/**
 *
 * @author user
 */
public class Q13Graph {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        // add the relation of Graph
//        Graph g = new Graph(4);
//        g.addEdge(0, 1);
//        g.addEdge(0, 2);
//        g.addEdge(1, 2);
//        g.addEdge(2, 0);
//        g.addEdge(2, 3);
        /* Outputs
           0: 0 1 1 0 
           1: 1 0 1 0 
           2: 1 1 0 1 
           3: 0 0 1 0 
        DFS start at 0
        DFS : 0 1 2 3 
        BFS start at 0
        BFS : 0 1 2 3 
         */
        
        Graph g = new Graph(5);
        // add the path of two point
        g.addEdge(0, 1);
        g.addEdge(0, 4);
        g.addEdge(1, 2);
        g.addEdge(1, 4);
        g.addEdge(1, 3);
        g.addEdge(2, 3);
        g.addEdge(3, 4);
        /*0: 0 1 0 0 1 
            1: 1 0 1 1 1 
            2: 0 1 0 1 0 
            3: 0 1 1 0 1 
            4: 1 1 0 1 0 
            DFS start at 0
            DFS : 0 1 2 3 4 
            BFS start at 0
            BFS : 0 1 4 2 3 */

        System.out.print(g.toString());
        System.out.println("");

        try {
            g.DFS(3);
            g.BFS(2);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
