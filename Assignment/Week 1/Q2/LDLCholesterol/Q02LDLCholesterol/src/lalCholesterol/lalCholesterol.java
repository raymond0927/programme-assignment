/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lalCholesterol;

/**
 *
 * @author itst
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class lalCholesterol extends JFrame implements ActionListener {

    /**
     * @param args the command line arguments
     */
    private JTextField jtfTriglyceride  = new JTextField(10);
    private JTextField jtfHDLcholesterol = new JTextField(10);
    private JTextField jtfTotalCholesterol = new JTextField(10);
    private JTextField jtfResult = new JTextField(20);
    private JButton jbCompute = new JButton("Compute");

    public static void main(String[] args) {
        // TODO code application logic here
        lalCholesterol frame = new lalCholesterol();
        frame.pack();
        frame.setTitle("LDL Cholesterol");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }

    public lalCholesterol() {
        // Panel p1 to hold labels and text fields
        JPanel p1 = new JPanel();
        p1.setLayout(new GridLayout(4, 2));
        p1.add(new JLabel("Total Cholesterol "));
        p1.add(jtfTotalCholesterol);
        p1.add(new JLabel("HDL cholesterol"));
        p1.add(jtfHDLcholesterol);
        p1.add(new JLabel("Triglyceride"));
        p1.add(jtfTriglyceride);
        p1.add(new JLabel("Result"));
        p1.add(jtfResult);
        jtfResult.setEditable(false);

        // Add p1 to the frame
        this.getContentPane().setLayout(new BorderLayout());
        this.getContentPane().add(p1, BorderLayout.CENTER);

        this.getContentPane().add(jbCompute, BorderLayout.SOUTH);

        // Register listener
        jbCompute.addActionListener(this);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == jbCompute) {

            try {
                double totalCholesterol = Double.parseDouble(jtfTotalCholesterol.getText().toString());

                double HDLcholesterol = Double.parseDouble(jtfHDLcholesterol.getText().toString());
                
                double triglyceride = Double.parseDouble(jtfTriglyceride.getText().toString());

                double LDLC = totalCholesterol - HDLcholesterol - triglyceride /5;

                jtfResult.setText(LDLC + "");
                
                //testing 50 -20 -10 / 5 = 28

            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(rootPane, "Input Error!");
            }

        } // end of if
    } // end of ActionPerformed
} // end Quiz2Q1

