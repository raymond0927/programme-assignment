/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package q12quicksort;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author user
 */
public class Q12QuickSort {

    /**
     * @param args the command line arguments
     */
    static List<Integer> list = new ArrayList<Integer>();
    
    public static void main(String[] args) {
        // add some number into list
        list.add(30);
        list.add(13);
        list.add(22);
        list.add(90);
        
        //do sorting
        list = Sort(list);
        for (int i =0 ; i <list.size();i++){
            System.out.print(list.get(i)+" ");
        }
    }
    
    public static List<Integer> Sort(List<Integer> list)
    {
        if (list.size() < 2){
            return list;
        }

        // find the middle to divide two group (less , greater)
        int middle = list.get(list.size() / 2);
        list.remove(list.size() / 2);
        
        List<Integer> less = new ArrayList<Integer>();
        List<Integer> greater = new ArrayList<Integer>();
        List<Integer> result = new ArrayList<Integer>();
        for (Integer n : list)
        {
            if (n > middle)
                greater.add(n);
            else
                less.add(n);
        }
        
        // recall the Sort method until the sort is ascending
        result.addAll(Sort(less));
        result.add(middle);
        result.addAll(Sort(greater));
        return result;
    }
    
}
