/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listqueue;

/**
 * Title: TestQueue2.java Description: A test drive program for the Queue class
 * Used SimpleInput class methods for inputs Company: ICT HKIVE(TY)
 *
 * @author Patrick Tong
 */
import miscLib.SimpleInput;
import miscLib.GenLib;

public class TestQueue {

    public static void main(String[] args) {
        ListQueue q = new ListQueue();
//		ArrayQueue q = new ArrayQueue(20);
        byte barray[] = new byte[20];
        int n;
        do {
            n = SimpleInput.getInteger("How many to enqueue?  -1 to exit");
            if (n == -1) {
                System.out.println("Bye now.");
                System.exit(0);
            } else {
                for (int i = 0; i < n; i++) {
                    q.enqueue(new Integer(GenLib.genInt(1, 100)));
                    System.out.println(q);
                }
            }

            n = SimpleInput.getInteger("How many to dequeue?   -1 to exit");
            if (n == -1) {
                System.out.println("Bye now.");
                System.exit(0);
            } else {
                for (int i = 0; i < n; i++) {
                    if (q.empty()) {
                        System.out.println("Queue is now empty, dequeue ignored.");
                        break;
                    } else {
                        System.out.println("dequeued:" + q.dequeue());
                        System.out.println(q);
                    }
                }
            }
        } while (SimpleInput.getChar("more (y/n)? ") == 'y');

        System.out.println("Bye now.");
        System.exit(0);
    }

} // clas TestQueue
