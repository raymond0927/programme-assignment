/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listqueue;

/**
 * Title:        SimBusStop.java
 * Description:  A simple Bus Stop simulation using Queue
 * Company:      ICT HKIVE(TY)
 * @author       Patrick Tong
 */

import miscLib.*;

public class SimBusStop {
//
//    public static void main(String[] args) {
////    ListQueue t = new ListQueue();
//	ArrayQueue t = new ArrayQueue(50);
//    int code, time, count;
//    Integer item;
//    int people = 0;
//    int bus =0;
//    int seats = 0;
//    int wait = 0;
//
//		do {
//			code = SimpleInput.getInteger("Code (0 for people, 1 for bus, -1 to exit): ");
//		} while (code < -1 || code > 1);
//		int lastTime = 0;
//		while (code!=-1) {
//			do {
//				time = SimpleInput.getInteger("Arrival time (>="+Integer.toString(lastTime)+"): ");
//			} while (lastTime > time);
//			lastTime = time;
//			do {
//				count = SimpleInput.getInteger("Arrival count (> 0): ");
//			} while (count <= 0);
//			if (code==0) {
//				people += count;
//				while (count-- > 0)
//					t.enqueue (new Integer(time));
//			}
//			else if (code==1) {
//                            bus ++;
//                            seats += count;
//				while ( (count-- > 0) && !t.empty ()){
//					item = (Integer) t.dequeue ();
//					wait += time - item.intValue();
//				}
//			}
//			do {
//				code = SimpleInput.getInteger("Code (0 for people, 1 for bus, -1 to exit): ");
//			} while (code < -1 || code > 1);
//		}
//
//		if (people!=0)
//			System.out.println (people + " people waited for " +
//								wait + " minutes, average waiting time = " +
//								(float) wait/people + " minutes."+
//                                bus +"of buses passed the bus stop; "+"Average number of people got on a bus = "+ people/bus +
//                                "Average number of vacant seats for a bus leaving the bus stop = "+ (seats - people)/bus +
//                                "The number of people still waiting at the bus stop, if there is any and their average\n" +
//"waiting time till the last input time =");
//	} // main

} // class SimBusStop