/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listqueue;

/**
 *
 * @author mancheng
 */
public class ListQueue extends LinkedList {

    /**
     * @param args the command line arguments
     */
    int sizeLimit = 10;

    public ListQueue() {    // <== constructor, different from ListStackComp.java
        super();
    }

    public boolean empty() {
        return this.isEmpty();
    }

    public boolean full() {
        return this.length == sizeLimit;
    }

    public void enqueue(Object item) {
        if (empty()) {
            this.head = this.tail = new ListNode(item);
        } else {
//            this.tail = new ListNode(item);
//            this.tail.setNext(new ListNode(item));
//            this.tail.setData(item);
            this.addToTail(item);
        }

    }

    public Object dequeue() throws EmptyQueueExceptions{
        Object item = null;
        if (empty()) {
            throw new EmptyQueueExceptions();
        } else {
            item = this.head.getData();
            if (this.head.getData() == this.tail.getData()) {
//                this.head.setData(null);
//                this.tail.setData(null);
                this.removeFromHead();
            } else {
//                this.head.setData(this.head.getNext());
                this.removeFromHead();
            }

        }
        return item;

    }
}
