package liststack;

/**
 * Title: ListStack.java Description: A Stack class by composition using
 * LinkedList object Company: ICT HKIVE(TY)
 *
 * @author Patrick Tong
 */

import java.util.EmptyStackException;

public class ListStackComp {

    private LinkedList list;	// compose with a LinkedList object

    public ListStackComp() {   // <== constructor, different from ListStack.java
        list = new LinkedList();
    }

    public boolean empty() {
        return list.isEmpty();
    }

    public Object push(Object item) {
        list.addToTail(item);

        return item;
    }

    public Object pop() {
        if (!empty()) {
            Object item = list.tail.getData();
            list.removeFromTail();
            return item;
        } else {
            throw new EmptyStackException();
        }
    }

    public Object peek() {
        if (!empty()) {
            return list.tail.getData();
        } else {
            throw new EmptyStackException();
        }
    }

    public int search(Object item) {
        if (!empty()) {
            boolean isSearched = false;
            int size = 0;
            int count = -1;
            ListNode current = list.head;
            while (current != null) {
                size++;
                if (current.getData().equals(item)) {
                    count = size - 1;
                    isSearched = true;
                }
                current = current.getNext();
            }
            if (isSearched) {
                return size - count;
            } else {
                return -1;
            }

        } else {
//            throw new EmptyStackException();
            return -1;
        }
    }

    // Provide also the toString() method (for TestStack.java)
    public String toString() {
        String ans = "[";
        ListNode current = list.head;
        while (current != null){
            ans += " "+current.getData();
            current = current.getNext();
        }
        return ans+" ]";
    }

}
