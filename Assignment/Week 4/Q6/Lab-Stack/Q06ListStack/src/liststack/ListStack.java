package liststack;

/**
 * Title: ListStack.java Description: A Stack class extended from LinkedList
 * class Company: ICT HKIVE(TY)
 *
 * @author Patrick Tong
 */
import java.util.EmptyStackException;

public class ListStack extends LinkedList {

    public ListStack() {    // <== constructor, different from ListStackComp.java
        super();
    }

    public boolean empty() {
        return this.isEmpty();
    }

    public Object push(Object item) {

        this.addToTail(item);

        return item;
    }

    public Object pop() {

        if (!empty()) {
            Object item = tail.getData();
            this.removeFromTail();
            return item;
        } else {
            throw new EmptyStackException();
        }

    }

    public Object peek() {
        if (!empty()) {
            return this.tail.getData();
        } else {
            throw new EmptyStackException();
        }
    }

    public int search(Object item) {
        if (!empty()) {
            boolean isSearched = false;
            int size = 0;
            int count = -1;
            ListNode current = head;
            while (current != null) {
                size++;
                if (current.getData().equals(item)) {
                    count = size-1;
                    isSearched = true;
                }
                current = current.getNext();
            }
            if (isSearched) {
                return size - count;
            } else {
                return -1;
            }

        } else {
//            throw new EmptyStackException();
            return -1;
        }
    }
}
