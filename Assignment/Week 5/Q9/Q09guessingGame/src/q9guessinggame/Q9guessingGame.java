/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package q9guessinggame;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author user
 */
public class Q9guessingGame extends JFrame implements ActionListener {

    /**
     * @param args the command line arguments
     */
    private JButton jbHigh = new JButton("The ans is higher");
    private JButton jbLow = new JButton("The ans is lower");
    private JButton jbBingo = new JButton("Bingo!");
    private JTextField jtfResult = new JTextField(20);

    public static int low = 0;
    public static int high = 0;
    public static int guessNumber = 0;
    public static List<Integer> inorder_list = new ArrayList();

    public static void main(String[] args) {
        // TODO code application logic here

        for (int i = 0; i < 2000; i++) {
            inorder_list.add(i + 1);
        }

        low = (int) inorder_list.get(0);
        high = (int) inorder_list.get(inorder_list.size() - 1);

        Q9guessingGame frame = new Q9guessingGame();
        frame.pack();
        frame.setTitle("Guessing Game");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        
    }

    public Q9guessingGame() {
        // Panel p1 to hold labels and text fields
        JPanel p1 = new JPanel();
        p1.setLayout(new GridLayout(4, 2));
        p1.add(new JLabel("Result"));
        p1.add(jtfResult);
        jtfResult.setEditable(false);

        // Add p1 to the frame
        this.getContentPane().setLayout(new BorderLayout());
        this.getContentPane().add(p1, BorderLayout.NORTH);

        this.getContentPane().add(jbHigh, BorderLayout.CENTER);
        this.getContentPane().add(jbLow, BorderLayout.WEST);
        this.getContentPane().add(jbBingo, BorderLayout.EAST);

        // Register listener
        jbHigh.addActionListener(this);
        jbLow.addActionListener(this);
        jbBingo.addActionListener(this);
        
        jtfResult.setText("Guess number :" +calculate());
        guessNumber = calculate();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // button listener
        if (e.getSource() == jbHigh) { 
            low = guessNumber;
            jtfResult.setText("Guess number :" +calculate());
            guessNumber = calculate();
        }else if (e.getSource() == jbLow) {
            high = guessNumber;
            jtfResult.setText("Guess number :" +calculate());
            guessNumber = calculate();
        }else if (e.getSource() == jbBingo) {
            jtfResult.setText("Bingo!");
        }
    }
    
    public int calculate(){
        // find the middle
        return (low + high) /2;
    }

//    public int search(int guessNum) {
//        int left = 0, right = inorder_list.size() - 1;
//        while (left <= right) {
//            int middle = (right + left) / 2;
//
//            if (inorder_list.get(middle) == guessNum) {
////                System.out.println("middle : true");
//                return middle;
//            }
//
////            System.out.println("inorder_list.get(middle) : " + inorder_list.get(middle) + "  guessNum  : " + guessNum);
//            if (inorder_list.get(middle) > guessNum) {
//                right = middle - 1;
////                System.out.println("right : " + right);
//            } else {
//                left = middle + 1;
////                System.out.println("left : " + left);
//            }
//        }
//        return -1;
//    }
//
//    public Boolean cofirmAns(int ans, int guessNum) {
//        int guessNum_position = search(guessNum);
//        System.out.println(guessNum_position + "  guessNum_position  " + inorder_list.size() + "  inorder_list.size()");
//        if (guessNum_position > -1) {
//            if (ans == guessNum) {
//                System.out.println("cofirmAns : true");
//                return true;
//            } else if (guessNum > ans) {
//                final int size = inorder_list.size();
//                high = guessNum;
//                for (int i = 0; i < size - guessNum_position - 1; i++) {
//                    inorder_list.remove(inorder_list.size() - 1);
//                }
//                System.out.println("cofirmAns : false + " + inorder_list.get(inorder_list.size() - 1));
//                return false;
//            } else {
//                low = guessNum;
//                for (int i = 0; i < guessNum_position; i++) {
//                    inorder_list.remove(0);
//                }
//                System.out.println("cofirmAns : false +first " + inorder_list.get(0));
//                return false;
//            }
//        } else {
//            System.out.println("cofirmAns2 : false");
//            return false;
//        }
//    }

}
