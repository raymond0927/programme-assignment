/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package linkedlist;

/**
 *
 * @author itst
 */
class LinkedList {

    private ListNode head;
    private ListNode tail;

    public LinkedList() {
        head = tail = null;
    }

    public boolean isEmpty() {
        return head == null;
    }

    public void addToHead(Object item) {
        if (isEmpty()) {
            head = tail = new ListNode(item);
        } else {
            head = new ListNode(item, head);
        }
    }

    public void addToTail(Object item) {
        if (isEmpty()) {
            head = tail = new ListNode(item);
        } else {
//            tail =  new ListNode(item);
            tail.setNext(new ListNode(item));
            while ((tail.getNext() != null)) {
                tail = tail.getNext();
            }
//            System.out.println("head " + head.getData());
//            System.out.println("tail " + tail.getData());
        }
    }

    public Object removeFromHead() throws EmptyListException {
        Object item = null;
        if (isEmpty()) {
            throw new EmptyListException();
        }

        if (head == tail) {
            //            item = tail.data;
            item = head.getData();
            head = tail = null;
        } else {
//            item = head.data;
            item = head.getData();
//            head = head.next;
            head = head.getNext();
        }

        return item;
    }

    public Object removeFromTail() throws EmptyListException {
        Object item = null;
        if (isEmpty()) {
            throw new EmptyListException();
        }

        if (head == tail) {
//            item = tail.data;
            item = head.getData();
            head = tail = null;
        } else {
            ListNode current = head;
            while (current.getNext() != tail) {
                current = current.getNext();
            }

//            item = current.next.data;
            item = current.getNext().getData();
//            current.next = null;
            current.setNext(null);
            tail = current;
        }

        return item;
    }

    public String toString() {
        String s = "[ ";
        ListNode current = head;
        while (current != null) {
            s += current.getData() + " ";
            current = current.getNext();
        }
        return s + "]";
    }
} // class LinkedList
