/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package linkedlist;

/**
 *
 * @author itst
 */
class ListNode {

    private Object data;
    private ListNode next;

    ListNode(Object o) {
        data = o;
        next = null;
    }

    ListNode(Object o, ListNode nextNode) {
        data = o;
        next = nextNode;
    }

    Object getData() {
        return data;
    }

    ListNode getNext() {
        return next;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public void setNext(ListNode next) {
        this.next = next;
    }

} // class ListNode
